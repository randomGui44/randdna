#include <string>
#include <random>

using std::string;

string randDNA(int s, string b, int n) {
	
	std::string result;
	std::mt19937 eng(s);
	
	std::uniform_int_distribution<int> uniform(0, b.size()-1);
	
	for(auto counter = 0; counter <= n-1; counter++){
		int arrangement = uniform(eng);
		result += b[arrangement];
	}//End For

	return result;
}//End Main
